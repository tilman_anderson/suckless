# Suckless

This repository hosts custom builds of several suckless programs as listed on the suckless website at https://suckless.org. These builds are customized with sane patches and defaults.

## Development state

The builds included in this repository may be outdated and not on the newest version of the program.

## Dependencies

Most of the builds in this repository will require the terminus font to be installed on the system as well as the dependencies stated in the README file associated with the specific program you plan to build.

## License

This repository is licensed under the MIT license as shown in the LICENSE associated with the repository.
